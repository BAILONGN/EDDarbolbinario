
import java.util.Scanner;

public class Arbolb { //el método puede ser accedido desde cualquier otro método que tenga una instancia de esta clase

    
    
    public static NodoArbol ingresoNodo(){//Para ingresar el nodo por teclado
        //int llave = (int) (Math.random() * 100) + 1;
        Scanner teclado=new Scanner(System.in); //provee métodos para leer valores de entrada de varios tipos y está localizada en el paquete java.util
        System.out.println("Ingrese llave"); //nos permite imprimir algo por pantalla en una ventana de consola.
        int llave=teclado.nextInt();
        System.out.println("Ingrese Elemento a guardar en el árbol: "); //nos permite imprimir algo por pantalla en una ventana de consola.
        String valor=teclado.nextLine(); //accede a través de las funciones miembro de la clase
        valor=teclado.nextLine();
        NodoArbol nodoNuevo=new NodoArbol(llave,valor); //nodo arbol
        return nodoNuevo; //retorno de valor de nodoNuevo
        
    }

    public static void main(String[] args) { // TODO code application logic here
        NodoArbol arbol=null; //null
        NodoArbol nodoNuevo; //nodoNuevo
        Scanner teclado=new Scanner(System.in); //provee métodos para leer valores de entrada de varios tipos y está localizada en el paquete java.util
        int opcion;
        //Insertar, buscar, modificar, eliminar, imprimir
        //estaVacia, Salir
        
        do{ //// Primero realiza las acciones luego pregunta
            System.out.println("\nIngrese opción: "+
                                "\n 1. Insertar nodo en árbol"+
                                " 2. Imprimir árbol en amplitud"+
                                "\n 3. Impresión Inorden "+
                                " 4. Impresión Preorden"+
                                "\n 5. Impresión Postorden"+
                                " 6. Eliminar arbol"+
                                " 7. Salir"); //nos permite imprimir algo por pantalla en una ventana de consola.
            opcion=teclado.nextInt();
            switch(opcion)
            {
                case 1: nodoNuevo=ingresoNodo();
                        arbol=NodoArbol.insertarArbol(nodoNuevo, arbol);
                        System.out.println("Ingreso satisfactorio..."); //nos permite imprimir algo por pantalla en una ventana de consola.
                        break;
                case 2: System.out.println("Imprimiendo árbol en amplitud \n");//nos permite imprimir algo por pantalla en una ventana de consola.
                        NodoArbol.Anchura2(arbol);
                        break;
                case 3: System.out.println("Imprimiendo árbol en Inorden \n"); //nos permite imprimir algo por pantalla en una ventana de consola.
                        NodoArbol.Inorden(arbol);
                        break;
                case 4: System.out.println("Imprimiendo árbol en PreOrden \n"); //nos permite imprimir algo por pantalla en una ventana de consola.
                        NodoArbol.Preorden(arbol);
                        break;
                case 5: System.out.println("Imprimiendo árbol en PostOrden \n"); //nos permite imprimir algo por pantalla en una ventana de consola.
                        NodoArbol.Postorden(arbol);
                        break;
                case 6: arbol=null;
                        System.out.println("Árbol eliminado \n"); //nos permite imprimir algo por pantalla en una ventana de consola.
                        
                        break;  
            }
            
        }while(opcion!=7); // indica que La iteración continuará hasta que su condición sea falsa.
    }
    
}

 class NodoArbol { //creacion de clase NodoArbol
    int llave;
    String valor;
    NodoArbol hijoIzquierdo;
    NodoArbol hijoDerecho;
    
    public NodoArbol(int llave, String valor){
        this.llave=llave;
        this.valor=valor;
        hijoIzquierdo=null; // hijo izquierdo de arbol binario
        hijoDerecho=null;   // hijo drecho de arbol binario
    }
    
    public static NodoArbol insertarArbol(NodoArbol nodoNuevo, NodoArbol arbol){
        //Partiendo de la raíz preguntamos: arbol == null ( o no existe )
        if ( arbol == null )  // En caso afirmativo X pasa a ocupar el lugar del nodo y ya hemos ingresado nuestro primer dato. 
          arbol=nodoNuevo;
        else { // sirve para indicar instrucciones a realizar en caso de no cumplirse la condición
            //preguntamos si el nodo nuevo, tiene una llave menor que la de la raiz
            if ( nodoNuevo.llave <= arbol.llave) // permite controlar qué procesos tienen lugar 
                arbol.hijoIzquierdo=NodoArbol.insertarArbol(nodoNuevo, arbol.hijoIzquierdo);
            else // sirve para indicar instrucciones a realizar en caso de no cumplirse la condición
                arbol.hijoDerecho=NodoArbol.insertarArbol(nodoNuevo, arbol.hijoDerecho);
        }
        return arbol;
    }
    
     public static void Anchura2 (NodoArbol Nodo){
     
     
  NodoCola cola= null;
  NodoArbol T = null;
  System.out.print ("El recorrido en Anchura es: "); //nos permite imprimir algo por pantalla en una ventana de consola.
  if(Nodo != null){ // permite controlar qué procesos tienen lugar
   cola=NodoCola.encolar (Nodo, cola);
   while(!(NodoCola.estaVacia(cola))){ // indica que La iteración continuará hasta que su condición sea falsa.
    T = new NodoArbol(cola.elemento.llave,cola.elemento.valor);
    cola=NodoCola.desencolar(cola);
    System.out.print(T.valor + " ");
    if (T.hijoIzquierdo != null) // permite controlar qué procesos tienen lugar
     cola=NodoCola.encolar (T.hijoIzquierdo,cola);
    if (T.hijoDerecho != null) // permite controlar qué procesos tienen lugar
     cola=NodoCola.encolar (T.hijoDerecho,cola);
   }
  }
  System.out.println();
 }

    
    public static void Inorden( NodoArbol nodo) //el método puede ser accedido desde cualquier otro método que tenga una instancia de esta clase
    {
        if(nodo!=null) // permite controlar qué procesos tienen lugar
        {
            Inorden(nodo.hijoIzquierdo);
            //System.out.print("\nLlave: {"+nodo.llave+"} = "+nodo.valor);
            System.out.print(nodo.valor+", ");
            Inorden(nodo.hijoDerecho);
        } 
    }
    
    public static void Preorden( NodoArbol nodo) //el método puede ser accedido desde cualquier otro método que tenga una instancia de esta clase
    {
        if(nodo!=null) // permite controlar qué procesos tienen lugar
        {
            System.out.print(nodo.valor+", ");
            Preorden(nodo.hijoIzquierdo);
            Preorden(nodo.hijoDerecho);
        }
         
        
    }
    
    public static void Postorden( NodoArbol nodo) //el método puede ser accedido desde cualquier otro método que tenga una instancia de esta clase
    {
        if(nodo!=null) // permite controlar qué procesos tienen lugar
        {
          Postorden(nodo.hijoIzquierdo);
          Postorden(nodo.hijoDerecho);
          System.out.print(nodo.valor+", ");
        }
        
    }
}
 class NodoCola { //creacion de clase NodoCola
    
    NodoArbol elemento;
    NodoCola siguiente;
    
    public NodoCola(NodoArbol elemento){
        this.elemento=elemento; //Contexto
        siguiente=null;
    }
    
    public NodoCola(){
        siguiente=null;
    }
    
     public static NodoCola encolar(NodoArbol arbol, NodoCola cola){
      
        NodoCola nodoNuevo = new NodoCola(arbol);
        if (cola==null)//CASO 1: Cuando la lista esta vacia
        {
            cola=nodoNuevo; //nodoNuevo
        }
        else //Tiene que funcionar insertando en la cima
        {
            NodoCola temporal=cola; //nodo temporal
            NodoCola anterior=cola;
            while(temporal!=null){ // indica que La iteración continuará hasta que su condición sea falsa.
                anterior=temporal;
                temporal=temporal.siguiente; //declaracion de variable
            }
            anterior.siguiente=nodoNuevo;
        }
        
        return cola;
    }
     
     public static void imprimir(NodoCola cola){
        NodoCola temporal=cola;
        if(cola==null) // permite controlar qué procesos tienen lugar
            System.out.println("Lista vacia... imposible imprimir"); //nos permite imprimir algo por pantalla en una ventana de consola.
        else // sirve para indicar instrucciones a realizar en caso de no cumplirse la condición
        {
            while(temporal!=null) // indica que La iteración continuará hasta que su condición sea falsa.
            {
                System.out.println("Llave: {"+temporal.elemento.llave+"} = "+temporal.elemento.valor);
                temporal=temporal.siguiente; // temporal
            }
        }
    }
     
      public static boolean estaVacia(NodoCola cola){ //  puede almacenar unicamente dos valores: verdadero o falso
        boolean bandera=true; // es una variable booleana que nos indica si ha ocurrido un suceso
        if(cola==null) // permite controlar qué procesos tienen lugar
            return bandera;
        return false; // return false
    }
      
       
       
       public static NodoCola desencolar(NodoCola cola){
           if(cola==null) // permite controlar qué procesos tienen lugar
            return null; //null
           
          
           cola=cola.siguiente;
           return cola; // valor de retorno
       }
       
      
}
